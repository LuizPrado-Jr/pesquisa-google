*** Settings ***
Resource    base.robot


***Keywords***

Dado que acesso a pagina de pesquisa do google
    Go To                    ${BASE_URL}
    Title Should Be          Google
    Capture Screen

Quando pesquiso por "${TEXTO_PESQUISAR}" 
    #[Arguments]         ${TEXTO_PESQUISAR}
    Click Element       ${CAIXA_GOOGLE} 
    Input Text          ${CAIXA_GOOGLE}     ${TEXTO_PESQUISAR}
    Press Keys          ${CAIXA_GOOGLE}     ENTER
    Capture Screen

Então válido se o retornou o site da ILab "${MENSAGEM_ESPERADA}" 

    Element Should Contain      ${SITE_ILAB}        ${MENSAGEM_ESPERADA}   
    Capture Screen
     

    