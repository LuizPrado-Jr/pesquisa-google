*** Settings ***
Library         SeleniumLibrary

Resource        base.robot

*** Variables ***
${NAVEGADOR}                 chrome


*** Keywords ***

Start Session
    Open Browser                    about:blank     chrome
    Set Window Size                 1920            1080
    Set Selenium Implicit Wait      10

Finish Session
    Close Browser

Capture Screen

    Capture Page Screenshot